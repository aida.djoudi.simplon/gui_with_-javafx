package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;

public class Controller {

    public Label label1;
    int totalApprenants = 8;

    public void addApprenant(ActionEvent actionEvent) {

        ++totalApprenants;
        label1.setText("Nombre des apprenants ALT :"+totalApprenants);

    }
    public void removeApprenant(ActionEvent actionEvent) {
        if(totalApprenants>0){
            --totalApprenants;
            label1.setText("Nombre des apprenants ALT :"+totalApprenants);
        }
    }

}
